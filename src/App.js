import React, {Component} from 'react';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            contacts: []
        };
    }

    componentDidMount() {
        fetch('http://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then((data) => {
                this.setState({contacts: data})
            })
            .catch(console.log)
    }

    render() {
        return (
            <div className="wrapper">
                   <select className="form-control">
                    <option>---select---</option>
                    {
                        this.state.contacts.map((h, i) =>
                            (<option key={i} value={h.id}>{h.name}</option>))
                    }
                </select>
            </div>

        );
    }
}

export default App;
